<%@ page import="am.example.spring.loginregister.model.User" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: annama
  Date: 5/27/2019
  Time: 11:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
</head>
<body>
<div>
    <h1>All users</h1>
    <%List<User> users = (List<User>)request.getAttribute("users");
    if (users != null && !users.isEmpty()) { %>
    <table>
        <% for (User user : users) {%>
        <tr>
            <td><%=user.getEmail()%></td>
            <td><%=user.getName()%></td>
            <td><a href="delete?id=<%=user.getId()%>">delete</a> </td>
        </tr>
        <% } %>
    </table>
    <%} %>
</div>
</body>
</html>
