package am.example.spring.loginregister.controller;

import am.example.spring.loginregister.dto.UserDto;
import am.example.spring.loginregister.model.User;
import am.example.spring.loginregister.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    private static final Logger LOGGER = Logger.getLogger(UserController.class);
    
//    private Logger LOGGER = Logger.getLogger(this.getClass());

    @RequestMapping("/")
    public String welcome() {
        return "index";
    }

    @GetMapping("/login")
    public String viewLogin(HttpServletRequest requset) {
        LOGGER.info("Get request '/login' ");
        if (requset.getSession().getAttribute("user") == null) {
            return "login";
        } else {
            return "redirect:home";
        }
    }

    @PostMapping("/login")
    public String login(@RequestParam("email") String email,
                        @RequestParam("password") String password,
                        HttpServletRequest requset) throws Exception {
        LOGGER.info("Post request '/login' ");
        UserDto currentUser = userService.login(email, password);
        if (currentUser != null) {
            requset.getSession().setAttribute("user", currentUser);
            return "redirect:home";
        } else {
            return "redirect:error";
        }
    }

    @GetMapping("/register")
    public String viewRegister(Model model, HttpServletRequest request) {
        LOGGER.info("Get request '/register' ");
        if (request.getSession().getAttribute("user") == null) {
            User user = new User();
            model.addAttribute("userForm", user);
            return "register";
        } else {
            return "redirect:home";
        }
    }

    @PostMapping("/register")
    public String register(@ModelAttribute("userForm") User user,
                           HttpServletRequest requset) throws Exception {
        LOGGER.info("Post request '/register' ");
        UserDto currentUser = userService.register(user);
        if (currentUser != null) {
            requset.getSession().setAttribute("user", currentUser);
            return "redirect:home";
        } else {
            return "redirect:error";
        }
    }

    @GetMapping("/home")
    public String getHome(HttpServletRequest requset) {
        LOGGER.info("Get request '/home' ");
        UserDto currentUser = (UserDto) requset.getSession().getAttribute("user");
        if (currentUser == null) {
            return "redirect:/";
        } else {
            return "home";
        }
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest requset) {
        LOGGER.info("Get request '/register' ");
        requset.getSession().removeAttribute("user");
        return "redirect:/";
    }

    @GetMapping("/error")
    public String error() {
        return "error";
    }

    @GetMapping("/delete")
    public String delete(@PathVariable int id) throws Exception {
        LOGGER.info("User with id:" + id + " was deleted" );
        userService.delete(id);
        return "redirect:admin";
    }

    @GetMapping("/admin")
    public String admin(){
        return "admin";
    }
}