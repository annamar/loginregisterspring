package am.example.spring.loginregister.service.impl;

import am.example.spring.loginregister.dao.UserDao;
import am.example.spring.loginregister.dto.UserDto;
import am.example.spring.loginregister.model.User;
import am.example.spring.loginregister.service.UserService;
import am.example.spring.loginregister.validation.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    UserValidator userValidator;

    public UserDto login(String email, String password) throws Exception {

        User user = userDao.getUserByEmail(email);
        if (user != null && password.equals(user.getPassword())) {
            return user.getUserDto();
        }
        return null;
    }

    public UserDto register(User user) throws Exception {

        if (userValidator.validateUser(user) && userDao.getUserByEmail(user.getEmail()) == null && userDao.add(user)) {
            return user.getUserDto();
        }
        return null;
    }

    @Override
    public void delete(int id) throws Exception {
        userDao.delete(id);
    }
}