package am.example.spring.loginregister.service;

import am.example.spring.loginregister.dto.UserDto;
import am.example.spring.loginregister.model.User;

public interface UserService {

    UserDto login(String username, String password) throws Exception;

    UserDto register(User user) throws Exception;

    void delete(int id) throws Exception;
}