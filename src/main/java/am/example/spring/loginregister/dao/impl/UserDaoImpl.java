package am.example.spring.loginregister.dao.impl;

import am.example.spring.loginregister.dao.UserDao;
import am.example.spring.loginregister.model.Role;
import am.example.spring.loginregister.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public boolean add(User user) {

        String query = "INSERT INTO user (name, email, password,role) VALUES (?,?,?,?)";
        Object[] args = new Object[]{user.getName(), user.getEmail(), user.getPassword(),user.getRole().toString()};

        int out = jdbcTemplate.update(query, args);

        return out != 0 ? true : false;
    }

    public User getUserByEmail(String email) {
        String query = "SELECT id, name, email,password FROM user where email = ?";
        Object[] args = new Object[]{email};

        try {
            User user = jdbcTemplate.queryForObject(query, args, new RowMapper<User>() {
                @Override
                public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                    User user = new User();
                    user.setId((rs.getInt("id")));
                    user.setName(rs.getString("name"));
                    user.setPassword(rs.getString("password"));
                    user.setEmail(rs.getString("email"));
                    user.setRole(Role.valueOf(rs.getString("role")));
                    return user;
                }
            });
            return user;
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
    }


    public List<User> getAll() {
        String query = "SELECT * FROM `user`";
        List<User> users = new ArrayList<User>();
        try {
            List<Map<String, Object>> userRows = jdbcTemplate.queryForList(query);
            for (Map<String, Object> userRow : userRows) {
                User user = new User();
                user.setId(Integer.parseInt(String.valueOf(userRow.get("id"))));
                user.setName(String.valueOf(userRow.get("name")));
                user.setEmail(String.valueOf(userRow.get("email")));
                user.setPassword(String.valueOf(userRow.get("password")));
                user.setRole(Role.valueOf(String.valueOf(userRow.get("role"))));

                users.add(user);
            }
            return users;
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void delete(int id) {

        String query = "delete from user where id=?";
        Object[] args = new Object[]{id};
        jdbcTemplate.update(query, args);
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
