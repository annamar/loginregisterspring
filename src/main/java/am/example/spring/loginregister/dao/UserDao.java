package am.example.spring.loginregister.dao;

import am.example.spring.loginregister.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {

    boolean add(User user);

    User getUserByEmail(String email);

    List<User> getAll();

    void delete(int id);

    void setJdbcTemplate(JdbcTemplate jdbcTemplate);
}
