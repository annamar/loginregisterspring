package am.example.spring.loginregister.validation;

import am.example.spring.loginregister.model.User;
import org.springframework.stereotype.Service;

@Service
public class UserValidator {
    public boolean validateUser(User user) {
        return user.getEmail().isEmpty() || user.getName().isEmpty() || user.getPassword().isEmpty() ? false : true;
    }
}
