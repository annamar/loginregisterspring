package am.example.spring.loginregister.dto;

import org.springframework.security.core.GrantedAuthority;

public class UserDto {
    private String name;
    private String email;
    private GrantedAuthority grantedAuthority;

    public UserDto(String name, String email, GrantedAuthority grantedAuthority) {
        this.name = name;
        this.email = email;
        this.grantedAuthority = grantedAuthority;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
