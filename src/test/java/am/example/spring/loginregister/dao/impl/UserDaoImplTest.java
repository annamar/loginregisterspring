package am.example.spring.loginregister.dao.impl;

import am.example.spring.loginregister.dao.UserDao;
import am.example.spring.loginregister.model.User;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.util.List;

import static org.junit.Assert.*;

public class UserDaoImplTest {

    private static UserDao userDao;

    @BeforeClass
    public static void init() {
        EmbeddedDatabase db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("create.sql")
                .build();
        userDao = new UserDaoImpl();
        JdbcTemplate template = new JdbcTemplate(db);
        userDao.setJdbcTemplate(template);
    }

    @Test
    public void addValidUser() {
        User user = new User("AAA", "aaa@mail.com", "aaa");
        boolean added = userDao.add(user);
        assertTrue(added);
        assertEquals(userDao.getAll().size(),3);
    }

    @Test
    public void getUserByEmail() {
        User user = userDao.getUserByEmail("poxos@mail.com");
        assertNotNull(user);
        assertEquals(user.getId(),1);
        assertEquals(user.getName(),"Poxos");
        assertEquals(user.getPassword(),"poxos");
    }

    @Test
    public void getAll() {
        List<User> all = userDao.getAll();
        assertEquals(all.size(),2);
    }
}